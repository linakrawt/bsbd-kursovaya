﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shop
{
    public partial class PurchaseHistory : Form
    {
        public PurchaseHistory()
        {
            InitializeComponent();
        }

        private void PurchaseHistory_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "shopDataSet1.ИсторияПокупок". При необходимости она может быть перемещена или удалена.
            this.историяПокупокTableAdapter.Fill(this.shopDataSet1.ИсторияПокупок);

        }
        public void ShowForm()
        {
            // toolStripButtonOk.Visible = false;
            // покупательBindingSource.Position = 0;
            Show();
            Activate();
        }
        private static PurchaseHistory f;
        public static PurchaseHistory fw
        {
            get
            {
                if (f == null || f.IsDisposed) f = new PurchaseHistory();
                return f;
            }
        }

        private void историяПокупокBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.историяПокупокBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.shopDataSet1);

        }
    }
}
