﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shop
{
    public partial class Features : Form
    {
        public Features()
        {
            InitializeComponent();
        }

        private void характеристикиBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.характеристикиBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.shopDataSet);

        }

        private void Features_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "shopDataSet.Характеристики". При необходимости она может быть перемещена или удалена.
            this.характеристикиTableAdapter.Fill(this.shopDataSet.Характеристики);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "shopDataSet.Характеристики". При необходимости она может быть перемещена или удалена.
            this.характеристикиTableAdapter.Fill(this.shopDataSet.Характеристики);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "shopDataSet.Характеристики". При необходимости она может быть перемещена или удалена.
            this.характеристикиTableAdapter.Fill(this.shopDataSet.Характеристики);

        }


        private static Features f;
        public static Features fw
        {
            get
            {
                if (f == null || f.IsDisposed) f = new Features();
                return f;
            }
        }
        public void ShowForm()
        {
            Show();
            Activate();
        }

        private void характеристикиDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void характеристикиBindingNavigatorSaveItem_Click_1(object sender, EventArgs e)
        {
            this.Validate();
            this.характеристикиBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.shopDataSet);

        }

        private void характеристикиBindingNavigatorSaveItem_Click_2(object sender, EventArgs e)
        {
            this.Validate();
            this.характеристикиBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.shopDataSet);

        }

        private void характеристикиDataGridView_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {

        }

        private void характеристикиBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void bindingNavigatorCountItem_Click(object sender, EventArgs e)
        {

        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {

        }

        private void bindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {

        }

        private void bindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {

        }

        private void bindingNavigatorSeparator_Click(object sender, EventArgs e)
        {

        }

        private void bindingNavigatorPositionItem_Click(object sender, EventArgs e)
        {

        }

        private void bindingNavigatorSeparator1_Click(object sender, EventArgs e)
        {

        }

        private void bindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {

        }

        private void bindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {

        }

        private void bindingNavigatorSeparator2_Click(object sender, EventArgs e)
        {

        }

        private void характеристикиBindingNavigator_RefreshItems(object sender, EventArgs e)
        {

        }
    }
}
