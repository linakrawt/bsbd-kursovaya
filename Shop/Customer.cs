﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shop
{
    public partial class Customer : Form
    {
        public Customer()
        {
            InitializeComponent();
        }

        private void покупательBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.покупательBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.shopDataSet1);

        }

        private void Customer_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "shopDataSet.Покупатель". При необходимости она может быть перемещена или удалена.
            this.покупательTableAdapter.Fill(this.shopDataSet1.Покупатель);

        }

        private void полTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void фотографияPictureBox_Click(object sender, EventArgs e)
        {

        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
        }

        string fileImage = "";
        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "Укажите файл для фото";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                fileImage = openFileDialog1.FileName;
                try
                {
                    фотографияPictureBox.Image = new
                    Bitmap(openFileDialog1.FileName);
                }
                catch
                {
                    MessageBox.Show("Выбран не тот формат файла", "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else fileImage = "";
        }



        public void ShowForm()
        {
            toolStripButtonOk.Visible = false;
            покупательBindingSource.Position = 0;
            Show();
            Activate();
        }


        private static Customer f;
        public static Customer fw
        {
            get
            {
                if (f == null || f.IsDisposed) f = new Customer();
                return f;
            }
        }

        private void toolStripButtonOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }


        //переменная для текущего(выбранного) кода сотрудника
        string idCurrent = "";
        public string ShowSelectForm(string id)
        {
            toolStripButtonOk.Visible = true;
            idCurrent = id;
            if (ShowDialog() == DialogResult.OK)
                return
                (string)((DataRowView)покупательBindingSource.Current)["Логин"];
            else
                return "";
        }

        private void Customer_Shown(object sender, EventArgs e)
        {
            покупательBindingSource.Position = покупательBindingSource.Find("Логин", idCurrent);
        }

        private void логинTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void покупательBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }
    }
}
    
