﻿namespace Shop
{
    partial class FormSQL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSQL));
            this.openFileDialogCustomer = new System.Windows.Forms.OpenFileDialog();
            this.tabPageDML = new System.Windows.Forms.TabPage();
            this.dataGridViewCustomer = new System.Windows.Forms.DataGridView();
            this.buttonSelectCustomer = new System.Windows.Forms.Button();
            this.panelCustomer = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.фотографияPictureBox = new System.Windows.Forms.PictureBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.категорияTextBox = new System.Windows.Forms.TextBox();
            this.labelSurname_customer = new System.Windows.Forms.Label();
            this.groupBoxDML = new System.Windows.Forms.GroupBox();
            this.buttonExecuteDML = new System.Windows.Forms.Button();
            this.textBoxId_customer = new System.Windows.Forms.TextBox();
            this.labelId_customer = new System.Windows.Forms.Label();
            this.radioButtonDelete_customer = new System.Windows.Forms.RadioButton();
            this.radioButtonUpdate_customer = new System.Windows.Forms.RadioButton();
            this.tabPageSelect = new System.Windows.Forms.TabPage();
            this.groupBoxFSelect = new System.Windows.Forms.GroupBox();
            this.dataGridViewFSelect = new System.Windows.Forms.DataGridView();
            this.buttonF_select = new System.Windows.Forms.Button();
            this.textBoxMore = new System.Windows.Forms.TextBox();
            this.checkBoxMore = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButtonDet_NO = new System.Windows.Forms.RadioButton();
            this.radioButtonDet_Category = new System.Windows.Forms.RadioButton();
            this.textBoxCustomer = new System.Windows.Forms.TextBox();
            this.labelCustomer = new System.Windows.Forms.Label();
            this.tabPagePrimer = new System.Windows.Forms.TabPage();
            this.groupBoxSelect = new System.Windows.Forms.GroupBox();
            this.dataGridViewSelect = new System.Windows.Forms.DataGridView();
            this.radioButtonProduct = new System.Windows.Forms.RadioButton();
            this.RadioButtonCustomer = new System.Windows.Forms.RadioButton();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabControlSQL = new System.Windows.Forms.TabControl();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.tabPageDML.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCustomer)).BeginInit();
            this.panelCustomer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.фотографияPictureBox)).BeginInit();
            this.groupBoxDML.SuspendLayout();
            this.tabPageSelect.SuspendLayout();
            this.groupBoxFSelect.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFSelect)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tabPagePrimer.SuspendLayout();
            this.groupBoxSelect.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSelect)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabControlSQL.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialogCustomer
            // 
            this.openFileDialogCustomer.FileName = "openFileDialog1";
            // 
            // tabPageDML
            // 
            this.tabPageDML.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(182)))), ((int)(((byte)(184)))));
            this.tabPageDML.Controls.Add(this.dataGridViewCustomer);
            this.tabPageDML.Controls.Add(this.buttonSelectCustomer);
            this.tabPageDML.Controls.Add(this.panelCustomer);
            this.tabPageDML.Controls.Add(this.groupBoxDML);
            this.tabPageDML.Location = new System.Drawing.Point(4, 22);
            this.tabPageDML.Name = "tabPageDML";
            this.tabPageDML.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageDML.Size = new System.Drawing.Size(1128, 530);
            this.tabPageDML.TabIndex = 3;
            this.tabPageDML.Text = "Изменение данных товара";
            // 
            // dataGridViewCustomer
            // 
            this.dataGridViewCustomer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCustomer.Location = new System.Drawing.Point(10, 398);
            this.dataGridViewCustomer.Name = "dataGridViewCustomer";
            this.dataGridViewCustomer.Size = new System.Drawing.Size(776, 124);
            this.dataGridViewCustomer.TabIndex = 3;
            // 
            // buttonSelectCustomer
            // 
            this.buttonSelectCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonSelectCustomer.Location = new System.Drawing.Point(792, 416);
            this.buttonSelectCustomer.Name = "buttonSelectCustomer";
            this.buttonSelectCustomer.Size = new System.Drawing.Size(268, 43);
            this.buttonSelectCustomer.TabIndex = 2;
            this.buttonSelectCustomer.Text = "Показать список товаров";
            this.buttonSelectCustomer.UseVisualStyleBackColor = true;
            this.buttonSelectCustomer.Click += new System.EventHandler(this.buttonSelectCustomer_Click);
            // 
            // panelCustomer
            // 
            this.panelCustomer.Controls.Add(this.button3);
            this.panelCustomer.Controls.Add(this.фотографияPictureBox);
            this.panelCustomer.Controls.Add(this.textBox5);
            this.panelCustomer.Controls.Add(this.label5);
            this.panelCustomer.Controls.Add(this.textBox3);
            this.panelCustomer.Controls.Add(this.label3);
            this.panelCustomer.Controls.Add(this.textBox4);
            this.panelCustomer.Controls.Add(this.label4);
            this.panelCustomer.Controls.Add(this.textBox1);
            this.panelCustomer.Controls.Add(this.label1);
            this.panelCustomer.Controls.Add(this.категорияTextBox);
            this.panelCustomer.Controls.Add(this.labelSurname_customer);
            this.panelCustomer.Location = new System.Drawing.Point(6, 144);
            this.panelCustomer.Name = "panelCustomer";
            this.panelCustomer.Size = new System.Drawing.Size(1110, 248);
            this.panelCustomer.TabIndex = 1;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button3.Location = new System.Drawing.Point(390, 106);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(119, 55);
            this.button3.TabIndex = 66;
            this.button3.Text = "Открыть фото";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // фотографияPictureBox
            // 
            this.фотографияPictureBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(86)))), ((int)(((byte)(123)))));
            this.фотографияPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.фотографияPictureBox.Location = new System.Drawing.Point(522, 61);
            this.фотографияPictureBox.Name = "фотографияPictureBox";
            this.фотографияPictureBox.Size = new System.Drawing.Size(182, 163);
            this.фотографияPictureBox.TabIndex = 65;
            this.фотографияPictureBox.TabStop = false;
            // 
            // textBox5
            // 
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox5.Location = new System.Drawing.Point(522, 20);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(182, 22);
            this.textBox5.TabIndex = 22;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(387, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 16);
            this.label5.TabIndex = 21;
            this.label5.Text = "id Характеристики";
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox3.Location = new System.Drawing.Point(137, 123);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(182, 22);
            this.textBox3.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(90, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 16);
            this.label3.TabIndex = 17;
            this.label3.Text = "Цена";
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox4.Location = new System.Drawing.Point(137, 171);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(182, 22);
            this.textBox4.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(55, 171);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 16);
            this.label4.TabIndex = 15;
            this.label4.Text = "Описание";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.Location = new System.Drawing.Point(137, 20);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(182, 22);
            this.textBox1.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(24, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 16);
            this.label1.TabIndex = 13;
            this.label1.Text = "Наименование";
            // 
            // категорияTextBox
            // 
            this.категорияTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.категорияTextBox.Location = new System.Drawing.Point(137, 68);
            this.категорияTextBox.Name = "категорияTextBox";
            this.категорияTextBox.Size = new System.Drawing.Size(182, 22);
            this.категорияTextBox.TabIndex = 3;
            // 
            // labelSurname_customer
            // 
            this.labelSurname_customer.AutoSize = true;
            this.labelSurname_customer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSurname_customer.Location = new System.Drawing.Point(55, 68);
            this.labelSurname_customer.Name = "labelSurname_customer";
            this.labelSurname_customer.Size = new System.Drawing.Size(76, 16);
            this.labelSurname_customer.TabIndex = 0;
            this.labelSurname_customer.Text = "Категория";
            // 
            // groupBoxDML
            // 
            this.groupBoxDML.Controls.Add(this.buttonExecuteDML);
            this.groupBoxDML.Controls.Add(this.textBoxId_customer);
            this.groupBoxDML.Controls.Add(this.labelId_customer);
            this.groupBoxDML.Controls.Add(this.radioButtonDelete_customer);
            this.groupBoxDML.Controls.Add(this.radioButtonUpdate_customer);
            this.groupBoxDML.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBoxDML.Location = new System.Drawing.Point(3, 6);
            this.groupBoxDML.Name = "groupBoxDML";
            this.groupBoxDML.Size = new System.Drawing.Size(990, 132);
            this.groupBoxDML.TabIndex = 0;
            this.groupBoxDML.TabStop = false;
            this.groupBoxDML.Text = "Операторы";
            // 
            // buttonExecuteDML
            // 
            this.buttonExecuteDML.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonExecuteDML.Location = new System.Drawing.Point(372, 75);
            this.buttonExecuteDML.Name = "buttonExecuteDML";
            this.buttonExecuteDML.Size = new System.Drawing.Size(205, 44);
            this.buttonExecuteDML.TabIndex = 13;
            this.buttonExecuteDML.Text = "Выполнить запрос";
            this.buttonExecuteDML.UseVisualStyleBackColor = true;
            this.buttonExecuteDML.Click += new System.EventHandler(this.buttonExecuteDML_Click);
            // 
            // textBoxId_customer
            // 
            this.textBoxId_customer.Location = new System.Drawing.Point(161, 78);
            this.textBoxId_customer.Name = "textBoxId_customer";
            this.textBoxId_customer.Size = new System.Drawing.Size(161, 22);
            this.textBoxId_customer.TabIndex = 4;
            // 
            // labelId_customer
            // 
            this.labelId_customer.AutoSize = true;
            this.labelId_customer.Location = new System.Drawing.Point(8, 81);
            this.labelId_customer.Name = "labelId_customer";
            this.labelId_customer.Size = new System.Drawing.Size(71, 16);
            this.labelId_customer.TabIndex = 3;
            this.labelId_customer.Text = "id Товара";
            // 
            // radioButtonDelete_customer
            // 
            this.radioButtonDelete_customer.AutoSize = true;
            this.radioButtonDelete_customer.Location = new System.Drawing.Point(372, 29);
            this.radioButtonDelete_customer.Name = "radioButtonDelete_customer";
            this.radioButtonDelete_customer.Size = new System.Drawing.Size(202, 20);
            this.radioButtonDelete_customer.TabIndex = 2;
            this.radioButtonDelete_customer.TabStop = true;
            this.radioButtonDelete_customer.Text = "Удалить данные по товару";
            this.radioButtonDelete_customer.UseVisualStyleBackColor = true;
            this.radioButtonDelete_customer.CheckedChanged += new System.EventHandler(this.radioButtonDelete_customer_CheckedChanged);
            // 
            // radioButtonUpdate_customer
            // 
            this.radioButtonUpdate_customer.AutoSize = true;
            this.radioButtonUpdate_customer.Location = new System.Drawing.Point(11, 29);
            this.radioButtonUpdate_customer.Name = "radioButtonUpdate_customer";
            this.radioButtonUpdate_customer.Size = new System.Drawing.Size(212, 20);
            this.radioButtonUpdate_customer.TabIndex = 1;
            this.radioButtonUpdate_customer.TabStop = true;
            this.radioButtonUpdate_customer.Text = "Изменить данные по товару";
            this.radioButtonUpdate_customer.UseVisualStyleBackColor = true;
            // 
            // tabPageSelect
            // 
            this.tabPageSelect.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(182)))), ((int)(((byte)(184)))));
            this.tabPageSelect.Controls.Add(this.groupBoxFSelect);
            this.tabPageSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabPageSelect.Location = new System.Drawing.Point(4, 22);
            this.tabPageSelect.Name = "tabPageSelect";
            this.tabPageSelect.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSelect.Size = new System.Drawing.Size(1128, 530);
            this.tabPageSelect.TabIndex = 1;
            this.tabPageSelect.Text = "Заказы покупателей";
            // 
            // groupBoxFSelect
            // 
            this.groupBoxFSelect.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(182)))), ((int)(((byte)(184)))));
            this.groupBoxFSelect.Controls.Add(this.dataGridViewFSelect);
            this.groupBoxFSelect.Controls.Add(this.buttonF_select);
            this.groupBoxFSelect.Controls.Add(this.textBoxMore);
            this.groupBoxFSelect.Controls.Add(this.checkBoxMore);
            this.groupBoxFSelect.Controls.Add(this.groupBox1);
            this.groupBoxFSelect.Controls.Add(this.textBoxCustomer);
            this.groupBoxFSelect.Controls.Add(this.labelCustomer);
            this.groupBoxFSelect.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxFSelect.Location = new System.Drawing.Point(3, 3);
            this.groupBoxFSelect.Name = "groupBoxFSelect";
            this.groupBoxFSelect.Size = new System.Drawing.Size(1122, 519);
            this.groupBoxFSelect.TabIndex = 0;
            this.groupBoxFSelect.TabStop = false;
            this.groupBoxFSelect.Text = "Заказы покупателей";
            this.groupBoxFSelect.Enter += new System.EventHandler(this.groupBoxFSelect_Enter);
            // 
            // dataGridViewFSelect
            // 
            this.dataGridViewFSelect.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFSelect.Location = new System.Drawing.Point(6, 193);
            this.dataGridViewFSelect.Name = "dataGridViewFSelect";
            this.dataGridViewFSelect.Size = new System.Drawing.Size(774, 320);
            this.dataGridViewFSelect.TabIndex = 6;
            // 
            // buttonF_select
            // 
            this.buttonF_select.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonF_select.Location = new System.Drawing.Point(348, 154);
            this.buttonF_select.Name = "buttonF_select";
            this.buttonF_select.Size = new System.Drawing.Size(88, 33);
            this.buttonF_select.TabIndex = 5;
            this.buttonF_select.Text = "Заказы";
            this.buttonF_select.UseVisualStyleBackColor = true;
            this.buttonF_select.Click += new System.EventHandler(this.buttonF_select_Click);
            // 
            // textBoxMore
            // 
            this.textBoxMore.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxMore.Location = new System.Drawing.Point(258, 99);
            this.textBoxMore.Name = "textBoxMore";
            this.textBoxMore.Size = new System.Drawing.Size(100, 26);
            this.textBoxMore.TabIndex = 4;
            // 
            // checkBoxMore
            // 
            this.checkBoxMore.AutoSize = true;
            this.checkBoxMore.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBoxMore.Location = new System.Drawing.Point(38, 99);
            this.checkBoxMore.Name = "checkBoxMore";
            this.checkBoxMore.Size = new System.Drawing.Size(214, 24);
            this.checkBoxMore.TabIndex = 3;
            this.checkBoxMore.Text = "Выбрать заказы дороже";
            this.checkBoxMore.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Controls.Add(this.radioButtonDet_NO);
            this.groupBox1.Controls.Add(this.radioButtonDet_Category);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(455, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(295, 133);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Детализация";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(26, 32);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(174, 20);
            this.radioButton1.TabIndex = 2;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Заказы по количеству";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButtonDet_NO
            // 
            this.radioButtonDet_NO.AutoSize = true;
            this.radioButtonDet_NO.Location = new System.Drawing.Point(26, 84);
            this.radioButtonDet_NO.Name = "radioButtonDet_NO";
            this.radioButtonDet_NO.Size = new System.Drawing.Size(141, 20);
            this.radioButtonDet_NO.TabIndex = 1;
            this.radioButtonDet_NO.TabStop = true;
            this.radioButtonDet_NO.Text = "Нет детализации";
            this.radioButtonDet_NO.UseVisualStyleBackColor = true;
            // 
            // radioButtonDet_Category
            // 
            this.radioButtonDet_Category.AutoSize = true;
            this.radioButtonDet_Category.Location = new System.Drawing.Point(26, 58);
            this.radioButtonDet_Category.Name = "radioButtonDet_Category";
            this.radioButtonDet_Category.Size = new System.Drawing.Size(160, 20);
            this.radioButtonDet_Category.TabIndex = 0;
            this.radioButtonDet_Category.TabStop = true;
            this.radioButtonDet_Category.Text = "Заказы по id Товара";
            this.radioButtonDet_Category.UseVisualStyleBackColor = true;
            // 
            // textBoxCustomer
            // 
            this.textBoxCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxCustomer.Location = new System.Drawing.Point(188, 51);
            this.textBoxCustomer.Name = "textBoxCustomer";
            this.textBoxCustomer.Size = new System.Drawing.Size(170, 26);
            this.textBoxCustomer.TabIndex = 1;
            // 
            // labelCustomer
            // 
            this.labelCustomer.AutoSize = true;
            this.labelCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCustomer.Location = new System.Drawing.Point(35, 51);
            this.labelCustomer.Name = "labelCustomer";
            this.labelCustomer.Size = new System.Drawing.Size(147, 20);
            this.labelCustomer.TabIndex = 0;
            this.labelCustomer.Text = "Логин покупателя";
            this.labelCustomer.Click += new System.EventHandler(this.labelCustomer_Click);
            // 
            // tabPagePrimer
            // 
            this.tabPagePrimer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(182)))), ((int)(((byte)(184)))));
            this.tabPagePrimer.Controls.Add(this.groupBoxSelect);
            this.tabPagePrimer.Location = new System.Drawing.Point(4, 22);
            this.tabPagePrimer.Name = "tabPagePrimer";
            this.tabPagePrimer.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePrimer.Size = new System.Drawing.Size(1128, 530);
            this.tabPagePrimer.TabIndex = 0;
            this.tabPagePrimer.Text = "Запросы по данным";
            // 
            // groupBoxSelect
            // 
            this.groupBoxSelect.Controls.Add(this.dataGridViewSelect);
            this.groupBoxSelect.Controls.Add(this.radioButtonProduct);
            this.groupBoxSelect.Controls.Add(this.RadioButtonCustomer);
            this.groupBoxSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBoxSelect.Location = new System.Drawing.Point(6, 6);
            this.groupBoxSelect.Name = "groupBoxSelect";
            this.groupBoxSelect.Size = new System.Drawing.Size(1114, 516);
            this.groupBoxSelect.TabIndex = 0;
            this.groupBoxSelect.TabStop = false;
            this.groupBoxSelect.Text = "Запросы по данным";
            this.groupBoxSelect.Enter += new System.EventHandler(this.groupBoxSelect_Enter);
            // 
            // dataGridViewSelect
            // 
            this.dataGridViewSelect.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSelect.Location = new System.Drawing.Point(6, 69);
            this.dataGridViewSelect.Name = "dataGridViewSelect";
            this.dataGridViewSelect.Size = new System.Drawing.Size(1102, 441);
            this.dataGridViewSelect.TabIndex = 3;
            // 
            // radioButtonProduct
            // 
            this.radioButtonProduct.AutoSize = true;
            this.radioButtonProduct.Location = new System.Drawing.Point(139, 32);
            this.radioButtonProduct.Name = "radioButtonProduct";
            this.radioButtonProduct.Size = new System.Drawing.Size(67, 20);
            this.radioButtonProduct.TabIndex = 1;
            this.radioButtonProduct.TabStop = true;
            this.radioButtonProduct.Text = "Товар";
            this.radioButtonProduct.UseVisualStyleBackColor = true;
            this.radioButtonProduct.CheckedChanged += new System.EventHandler(this.radioButtonProduct_CheckedChanged);
            // 
            // RadioButtonCustomer
            // 
            this.RadioButtonCustomer.AutoSize = true;
            this.RadioButtonCustomer.Location = new System.Drawing.Point(24, 32);
            this.RadioButtonCustomer.Name = "RadioButtonCustomer";
            this.RadioButtonCustomer.Size = new System.Drawing.Size(105, 20);
            this.RadioButtonCustomer.TabIndex = 0;
            this.RadioButtonCustomer.TabStop = true;
            this.RadioButtonCustomer.Text = "Покупатель";
            this.RadioButtonCustomer.UseVisualStyleBackColor = true;
            this.RadioButtonCustomer.CheckedChanged += new System.EventHandler(this.RadioButtonCustomer_CheckedChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(182)))), ((int)(((byte)(184)))));
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.textBox2);
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1128, 530);
            this.tabPage1.TabIndex = 4;
            this.tabPage1.Text = "Ввести свой запрос";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(14, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "SQL-Запрос";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button2.Location = new System.Drawing.Point(572, 127);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(122, 54);
            this.button2.TabIndex = 3;
            this.button2.Text = "Очистить поле";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(572, 59);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(122, 39);
            this.button1.TabIndex = 2;
            this.button1.Text = "Выполнить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(18, 59);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(535, 177);
            this.textBox2.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(18, 281);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(750, 226);
            this.dataGridView1.TabIndex = 0;
            // 
            // tabControlSQL
            // 
            this.tabControlSQL.Controls.Add(this.tabPage1);
            this.tabControlSQL.Controls.Add(this.tabPagePrimer);
            this.tabControlSQL.Controls.Add(this.tabPageSelect);
            this.tabControlSQL.Controls.Add(this.tabPageDML);
            this.tabControlSQL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlSQL.Location = new System.Drawing.Point(0, 0);
            this.tabControlSQL.Name = "tabControlSQL";
            this.tabControlSQL.SelectedIndex = 0;
            this.tabControlSQL.Size = new System.Drawing.Size(1136, 556);
            this.tabControlSQL.TabIndex = 0;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // FormSQL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(182)))), ((int)(((byte)(184)))));
            this.ClientSize = new System.Drawing.Size(1136, 556);
            this.Controls.Add(this.tabControlSQL);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormSQL";
            this.Text = "Запросы";
            this.Load += new System.EventHandler(this.FormSQL_Load);
            this.tabPageDML.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCustomer)).EndInit();
            this.panelCustomer.ResumeLayout(false);
            this.panelCustomer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.фотографияPictureBox)).EndInit();
            this.groupBoxDML.ResumeLayout(false);
            this.groupBoxDML.PerformLayout();
            this.tabPageSelect.ResumeLayout(false);
            this.groupBoxFSelect.ResumeLayout(false);
            this.groupBoxFSelect.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFSelect)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPagePrimer.ResumeLayout(false);
            this.groupBoxSelect.ResumeLayout(false);
            this.groupBoxSelect.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSelect)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabControlSQL.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog openFileDialogCustomer;
        private System.Windows.Forms.TabPage tabPageDML;
        private System.Windows.Forms.DataGridView dataGridViewCustomer;
        private System.Windows.Forms.Button buttonSelectCustomer;
        private System.Windows.Forms.Panel panelCustomer;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox категорияTextBox;
        private System.Windows.Forms.Label labelSurname_customer;
        private System.Windows.Forms.GroupBox groupBoxDML;
        private System.Windows.Forms.Button buttonExecuteDML;
        private System.Windows.Forms.TextBox textBoxId_customer;
        private System.Windows.Forms.Label labelId_customer;
        private System.Windows.Forms.RadioButton radioButtonDelete_customer;
        private System.Windows.Forms.RadioButton radioButtonUpdate_customer;
        private System.Windows.Forms.TabPage tabPageSelect;
        private System.Windows.Forms.GroupBox groupBoxFSelect;
        private System.Windows.Forms.DataGridView dataGridViewFSelect;
        private System.Windows.Forms.Button buttonF_select;
        private System.Windows.Forms.TextBox textBoxMore;
        private System.Windows.Forms.CheckBox checkBoxMore;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButtonDet_NO;
        private System.Windows.Forms.RadioButton radioButtonDet_Category;
        private System.Windows.Forms.TextBox textBoxCustomer;
        private System.Windows.Forms.Label labelCustomer;
        private System.Windows.Forms.TabPage tabPagePrimer;
        private System.Windows.Forms.GroupBox groupBoxSelect;
        private System.Windows.Forms.DataGridView dataGridViewSelect;
        private System.Windows.Forms.RadioButton radioButtonProduct;
        private System.Windows.Forms.RadioButton RadioButtonCustomer;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TabControl tabControlSQL;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox фотографияPictureBox;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}