﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Shop
{
    public partial class MainFormUser : Form
    {
        public MainFormUser()
        {
            InitializeComponent();
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void MainFormUser_Load(object sender, EventArgs e)
        {

        }

        private void MainFormUser_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = MessageBox.Show("Вы хотите закрыть программу?",
           "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Question) !=
            DialogResult.Yes;
        }

        private void справкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("(c) ТУСУР,КИБЭВС, Кравцова Эвелина Евгеньевна, группа 728-1, 2021", "О программе",
              MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void выходToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            выходToolStripMenuItem_Click(sender, e);
        }

        private void справкаToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            справкаToolStripMenuItem_Click(sender, e);
        }

        private void MainFormUser_ContextMenuStripChanged(object sender, EventArgs e)
        {

        }





        private void отзывыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Reviews.fw.ShowForm();
        }

        private void запросыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormSQL.fw.ShowForm();
        }

        private void покупательToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            CustomerUser.fw.ShowForm();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("(c) ТУСУР,КИБЭВС, Кравцова Эвелина Евгеньевна, группа 728-1, 2021", "О программе",
  MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void выходToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Close();
        }

        private void справкаToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            MessageBox.Show("(c) ТУСУР,КИБЭВС, Кравцова Эвелина Евгеньевна, группа 728-1, 2021", "О программе",
  MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void товарToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            ProductUser.fw.ShowForm();
        }

        private void характеристикиToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            FeaturesUser.fw.ShowForm();
        }

        private void отзывыToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            ReviewsUser.fw.ShowForm();
        }

        private void корзмнаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cart.fw.ShowForm();
        }

        private void contextMenuStrip2_Opening(object sender, CancelEventArgs e)
        {

        }
    }
}
