﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shop
{
    public partial class HistotyLog : Form
    {
        public HistotyLog()
        {
            InitializeComponent();
        }

        private void bindingSource1_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void историяПокупок_logBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.историяПокупок_logBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.shopDataSet1);

        }

        private void HistotyLog_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "shopDataSet1.ИсторияПокупок_log". При необходимости она может быть перемещена или удалена.
            this.историяПокупок_logTableAdapter.Fill(this.shopDataSet1.ИсторияПокупок_log);

        }

        public void ShowForm()
        {
           // toolStripButtonOk.Visible = false;
           // покупательBindingSource.Position = 0;
            Show();
            Activate();
        }


        private static HistotyLog f;
        public static HistotyLog fw
        {
            get
            {
                if (f == null || f.IsDisposed) f = new HistotyLog();
                return f;
            }
        }
    }
}
