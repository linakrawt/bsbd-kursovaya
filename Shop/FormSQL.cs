﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace Shop
{
    public partial class FormSQL : Form
    {
        public FormSQL()
        {
            InitializeComponent();
        }

        private void RadioButtonCustomer_CheckedChanged(object sender, EventArgs e)
        {
            dataGridViewSelect.DataSource = FillDataGridView("SELECT * FROM Покупатель");
        }

        private void FormSQL_Load(object sender, EventArgs e)
        {

        }

        class DB
        {
            SqlConnection connection = new SqlConnection(Properties.Settings.Default.ShopConnectionString);

            public void OpenConnection()
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                    connection.Open();
            }

            public SqlConnection GetConnection()
            {
                return connection;
            }

            public void CloseConnection()
            {
                if (connection.State == System.Data.ConnectionState.Open)
                    connection.Close();
            }
        }

        DataTable FillDataGridView(string sqlSelect)
        {
            //Создаем объект connection класса SqlConnection для соединения с БД
            //ShopConnectionString – строка описания соединения с источником данных
            SqlConnection connection = new
            SqlConnection(Properties.Settings.Default.ShopConnectionString);
            //Создаем объект command для SQL команды
            SqlCommand command = connection.CreateCommand();
            //Заносим текст SQL запроса через параметр sqlSelect
            command.CommandText = sqlSelect;
            //Создаем объект adapter класса SqlDataAdapter
            SqlDataAdapter adapter = new SqlDataAdapter();
            //Задаем адаптеру нужную команду, в данном случае команду Select
            adapter.SelectCommand = command;
            //Создаем объект table для последующего отображения результата запроса
            DataTable table = new DataTable();
            //заполним набор данных результатом запроса
            adapter.Fill(table);
            return table;
        }

        private static FormSQL f;
        public static FormSQL fw
        {
            get
            {
                if (f == null || f.IsDisposed) f = new FormSQL();
                return f;
            }
        }
        public void ShowForm()
        {
            Show();
            Activate();
        }

        private void radioButtonProduct_CheckedChanged(object sender, EventArgs e)
        {
            dataGridViewSelect.DataSource = FillDataGridView(@"SELECT id_Товара, Наименование+' '+Категория AS Подробнее, CAST(Цена AS decimal(16,5)) AS [Текущая цена] FROM Товар");
        }


        private void labelCustomer_Click(object sender, EventArgs e)
        {

        }

        private void buttonF_select_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBoxCustomer.Text))
            {
                MessageBox.Show("Обязательно укажите Логин необходимого сотрудника.\nДопустим ввод первых символов.", "Внимание", MessageBoxButtons.OK,
                MessageBoxIcon.Warning);
                return;
            }
            if (checkBoxMore.Checked && String.IsNullOrEmpty(textBoxMore.Text))
            {
                MessageBox.Show("Не указана сумма заказа в условии", "Внимание",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                checkBoxMore.Checked = false;
                return;
            }
            string sqlSelect = "";
            if (radioButton1.Checked)
            {
                sqlSelect = @"SELECT Логин, SUM(Стоимость) as [Сумма покупок], Количество FROM ИсторияПокупок WHERE Логин LIKE @login GROUP BY Логин, Количество";
            }
            else
            if (radioButtonDet_Category.Checked)
                sqlSelect = @"SELECT Логин, SUM(Стоимость) as [Сумма покупок], id_Товара FROM ИсторияПокупок WHERE Логин LIKE @login GROUP BY Логин, id_Товара";
            else
                sqlSelect = @"SELECT Логин, Стоимость as [Сумма покупок] FROM ИсторияПокупок WHERE Логин LIKE @login GROUP BY Логин, Стоимость";
            if (checkBoxMore.Checked)
                sqlSelect += " HAVING SUM(Стоимость) > @amount";
            SqlConnection connection = new
            SqlConnection(Properties.Settings.Default.ShopConnectionString);
            SqlCommand command = connection.CreateCommand();
            command.CommandText = sqlSelect;
            command.Parameters.AddWithValue("@login", textBoxCustomer.Text + "%");
            if (checkBoxMore.Checked)
                try
                {
                    command.Parameters.Add("@amount", SqlDbType.Money).Value =
                    Double.Parse(textBoxMore.Text);
                }
                catch
                {
                    MessageBox.Show("Сумма в условии должна быть задана числом", "ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    checkBoxMore.Checked = false;
                    return;
                }
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            DataTable table = new DataTable();
            adapter.Fill(table);
            dataGridViewFSelect.DataSource = table;
            if (table.Rows.Count == 0) MessageBox.Show("Нет значений!",
            "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }



        private void Почта_Click(object sender, EventArgs e)
        {

        }






        private void buttonSelectCustomer_Click(object sender, EventArgs e)
        {
            dataGridViewCustomer.DataSource = FillDataGridView("SELECT * FROM Товар");

        }

        void UpdateProduct()
        {

            try
            {
                DB db = new DB();
                db.OpenConnection();

                String Login = Auth.Personallogin;
                if (textBox1.Text == "" || категорияTextBox.Text == "" || textBox3.Text == "" || textBox4.Text == "" || textBox5.Text == "")
                {
                    MessageBox.Show("Заполнены не все обязательные поля!");
                    return;
                }


                SqlCommand command = new SqlCommand("SELECT id_Товара FROM Товар WHERE id_Товара = " + Convert.ToInt32(textBoxId_customer.Text) + " ", db.GetConnection());
                var Sql = command.ExecuteScalar();
                if (Sql == null)
                {
                    MessageBox.Show("Нет такого товара!");
                    return;
                }


                SqlCommand command1 = new SqlCommand("SELECT id_Характеристики FROM Характеристики WHERE id_Характеристики = " + textBox5.Text + " ", db.GetConnection());
                var Sql1 = command1.ExecuteScalar();
                if (Sql1 == null)
                {
                    MessageBox.Show("Нет такой характеристики!");
                    return;
                }

                string Name = textBox1.Text;
                string Cat = категорияTextBox.Text;
                float Price = float.Parse(textBox3.Text.ToString());
                string Summary = textBox4.Text;
                string idFeat = textBox5.Text;
                int idProduct = Convert.ToInt32(textBoxId_customer.Text);

                SqlCommand command2 = new SqlCommand("UPDATE Товар SET Наименование ='" + Name + "' WHERE id_Товара LIKE @idProduct", db.GetConnection());
                command2.Parameters.Add("@idProduct", SqlDbType.NVarChar).Value = idProduct;


                SqlCommand command3 = new SqlCommand("UPDATE Товар SET Категория ='" + Cat + "' WHERE id_Товара LIKE @idProduct", db.GetConnection());
                command3.Parameters.Add("@idProduct", SqlDbType.NVarChar).Value = idProduct;

                SqlCommand command4 = new SqlCommand("UPDATE Товар SET Цена = @Price WHERE id_Товара LIKE @idProduct", db.GetConnection());
                command4.Parameters.Add("@idProduct", SqlDbType.NVarChar).Value = idProduct;
                command4.Parameters.Add("@Price", SqlDbType.Money).Value = Price;

                SqlCommand command5 = new SqlCommand("UPDATE Товар SET Описание ='" + Summary + "' WHERE id_Товара LIKE @idProduct", db.GetConnection());
                command5.Parameters.Add("@idProduct", SqlDbType.NVarChar).Value = idProduct;

                SqlCommand command6 = new SqlCommand("UPDATE Товар SET id_Характеристики='" + idFeat + "' WHERE id_Товара LIKE @idProduct", db.GetConnection());
                command6.Parameters.Add("@idProduct", SqlDbType.NVarChar).Value = idProduct;


                if (фотографияPictureBox.Image != null)
                {
                    byte[] imageData = null;
                    using (var ms = new MemoryStream())
                    {
                        фотографияPictureBox.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                        imageData = ms.ToArray();
                    }
                    SqlCommand command11 = new SqlCommand("UPDATE Товар SET Фотография = @imageDat WHERE id_Товара LIKE @idProduct", db.GetConnection());
                    command11.Parameters.Add("@idProduct", SqlDbType.NVarChar).Value = idProduct;
                    command11.Parameters.Add("@imageDat", SqlDbType.VarBinary).Value = imageData;
                    var Sql11 = command11.ExecuteScalar();

                }

                var Sql2 = command2.ExecuteScalar();
                var Sql3 = command3.ExecuteScalar();
                var Sql4 = command4.ExecuteScalar();
                var Sql5 = command5.ExecuteScalar();
                var Sql6 = command6.ExecuteScalar();

               

                if (Convert.ToInt32(Sql2) != null|| Convert.ToInt32(Sql3) != null|| Convert.ToInt32(Sql4) != null|| Convert.ToInt32(Sql5) != null|| Convert.ToInt32(Sql6) != null)
                    MessageBox.Show("Изменения сохранены.");
                else MessageBox.Show("Что-то не так");
                db.CloseConnection();

        }
            catch (Exception err)
            {
                MessageBox.Show("Ошибка выполнения запроса:\n" + err.Message,
                "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

    buttonSelectCustomer_Click(this, EventArgs.Empty);
        }



        void DeleteProduct()
        {
            if (String.IsNullOrEmpty(textBoxId_customer.Text))
            {
                MessageBox.Show("Обязательно укажите id", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            return;
            }
            int id;
            if (!int.TryParse(textBoxId_customer.Text, out id))
            {
                MessageBox.Show("Некоректное значение id!", "Внимание",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            string sqlDelete = @"DELETE FROM Товар WHERE id_Товара = @Id";
            SqlConnection connection = new SqlConnection(Properties.Settings.Default.ShopConnectionString);
            connection.Open();
            SqlCommand command = connection.CreateCommand();
            command.CommandText = sqlDelete;
            command.Parameters.AddWithValue("@Id", id);
            try
            {
                command.ExecuteNonQuery();
                MessageBox.Show("Изменения сохранены.");
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "Ошибка удаления "+err.Message+"");
            }
            connection.Close();
            buttonSelectCustomer_Click(this, EventArgs.Empty);
        }

        private void buttonExecuteDML_Click(object sender, EventArgs e)
        {
            {

                 if (radioButtonUpdate_customer.Checked)
                    UpdateProduct();
                  else
                 if (radioButtonDelete_customer.Checked)
                    DeleteProduct();
                else
                    MessageBox.Show("Вы не выбрали действие", "Внимание",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void radioButtonDelete_customer_CheckedChanged(object sender, EventArgs e)
        {
            panelCustomer.Visible = !radioButtonDelete_customer.Checked;
        }

        private void groupBoxSelect_Enter(object sender, EventArgs e)
        {

        }

        private void groupBoxFSelect_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox2.Text != "")
                {
                    SqlConnection connection = new SqlConnection(Properties.Settings.Default.ShopConnectionString);
                    connection.Open();
                    SqlCommand command = connection.CreateCommand(); 


                    command.CommandText = textBox2.Text;
                    command.ExecuteNonQuery();
                    string selectSQL = "";
                    string sql = textBox2.Text;
                    for (int i = 0; i < 6; i++)
                    {
                        selectSQL += sql[i];
                    }
                    if (selectSQL=="select"|| selectSQL == "SELECT")
                    {
                        dataGridView1.DataSource = FillDataGridView(sql);
                    }
                    connection.Close();
                }
                else MessageBox.Show("Вы ничего не ввели!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Упс, что-то пошло не так! " + ex.Message + "", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
            string fileImage = "";
        private void button3_Click(object sender, EventArgs e)
        {


                openFileDialog1.Title = "Укажите файл для фото";
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    fileImage = openFileDialog1.FileName;
                    try
                    {
                        фотографияPictureBox.Image = new
                        Bitmap(openFileDialog1.FileName);
                    }
                    catch
                    {
                        MessageBox.Show("Выбран не тот формат файла", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                else fileImage = "";
            }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox2.Clear();
        }
    }
}
