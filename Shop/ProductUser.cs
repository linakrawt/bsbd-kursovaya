﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Shop.Cart;

namespace Shop
{

   
    public partial class ProductUser : Form
    {
        public static List<itemCart> cart = new List<itemCart>();

        public void ShowForm()
        {
            // toolStripButtonOk.Visible = false;
            // покупательBindingSource.Position = 0;
            Show();
            Activate();
        }
        private static ProductUser f;
        public static ProductUser fw
        {
            get
            {
                if (f == null || f.IsDisposed) f = new ProductUser();
                return f;
            }
        }

        public ProductUser()
        {
            InitializeComponent();
        }

        private void ProductUser_Load(object sender, EventArgs e)
        {

                // TODO: данная строка кода позволяет загрузить данные в таблицу "shopDataSet.Товар". При необходимости она может быть перемещена или удалена.
                this.товарTableAdapter.Fill(this.shopDataSet1.Товар);
                // TODO: данная строка кода позволяет загрузить данные в таблицу "shopDataSet.Товар". При необходимости она может быть перемещена или удалена.
                this.товарTableAdapter.Fill(this.shopDataSet1.Товар);
                // TODO: данная строка кода позволяет загрузить данные в таблицу "shopDataSet.Товар_Корзина". При необходимости она может быть перемещена или удалена.

                // TODO: данная строка кода позволяет загрузить данные в таблицу "shopDataSet.Отзывы". При необходимости она может быть перемещена или удалена.

                // TODO: данная строка кода позволяет загрузить данные в таблицу "shopDataSet.Товар". При необходимости она может быть перемещена или удалена.
                this.товарTableAdapter.Fill(this.shopDataSet1.Товар);
                // TODO: данная строка кода позволяет загрузить данные в таблицу "shopDataSet.Товар". При необходимости она может быть перемещена или удалена.
                this.товарTableAdapter.Fill(this.shopDataSet1.Товар);
                // TODO: данная строка кода позволяет загрузить данные в таблицу "shopDataSet.Товар". При необходимости она может быть перемещена или удалена.
                this.товарTableAdapter.Fill(this.shopDataSet1.Товар);


            
        }

        private void toolStripButtonFind_Click(object sender, EventArgs e)
        {
            if (toolStripTextBoxFind.Text == "")
            {
                MessageBox.Show("Вы ничего не задали", "Внимание",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            int indexPos;
            try
            {
                indexPos =
                товарBindingSource.Find(GetSelectedFieldName(),
                toolStripTextBoxFind.Text);
            }
            catch (Exception err)
            {
                MessageBox.Show("Ошибка поиска \n" + err.Message);
                return;
            }
            if (indexPos > -1)
                товарBindingSource.Position = indexPos;
            else
            {
                MessageBox.Show("Такого товара нет", "Внимание",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                товарBindingSource.Position = 0;
            }
        }

        string GetSelectedFieldName()
        {
            return
            товарDataGridView.Columns[товарDataGridView.CurrentCell.ColumnIndex
            ].DataPropertyName;
        }

        private void checkBoxFind_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxFind.Checked)
            {
                if (toolStripTextBoxFind.Text == "")
                    MessageBox.Show("Вы ничего не задали", "Внимание",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    try
                    {
                        товарBindingSource.Filter =
                        GetSelectedFieldName() + "='" + toolStripTextBoxFind.Text + "'";
                    }
                    catch (Exception err)
                    {
                        MessageBox.Show("Ошибка фильтрации \n" +
                        err.Message);
                    }
            }
            else
                товарBindingSource.Filter = "";
            if (товарBindingSource.Count == 0)
            {
                MessageBox.Show("Нет такого");
                товарBindingSource.Filter = "";
                checkBoxFind.Checked = false;
            }
        }
    
        
        private void товарDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        { 
   
            if (IsANonHeaderButtonCell(e))
            {
              int indexPos = товарBindingSource.Position;
                int a = Convert.ToInt32(товарDataGridView.Rows[indexPos].Cells[0].Value);
                float b = float.Parse(товарDataGridView.Rows[indexPos].Cells[3].Value.ToString());
                int c = Convert.ToInt32(товарDataGridView.Rows[indexPos].Cells[7].Value);
                //if (cart.Find(item => item.idProduct.Contains(a)) != null) 
                foreach (var Item in cart)
                {
                    if (Item.idProduct == a)
                    {
                        Item.Amount += c;
                        Item.Cost+= c*b;
                        MessageBox.Show("Вы добавили в корзину "+c+" единиц товара!");
                        return;
                    }
                   
                }
                cart.Add(new itemCart(a, b, c, b*c));
                MessageBox.Show("Товар добавлен в корзину!");

            }

         
        }

        private bool IsANonHeaderButtonCell(DataGridViewCellEventArgs cellEvent)
        {
            if (товарDataGridView.Columns[cellEvent.ColumnIndex] is DataGridViewButtonColumn && cellEvent.RowIndex != -1)
            { return true; }
            else { return (false); }
        }
    }
}
