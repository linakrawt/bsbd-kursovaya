﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shop
{
    public partial class Product : Form
    {
        public Product()
        {
            InitializeComponent();
        }

        private void товарBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.товарBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.shopDataSet1);

        }

        private void Product_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "shopDataSet.Товар". При необходимости она может быть перемещена или удалена.
            this.товарTableAdapter.Fill(this.shopDataSet1.Товар);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "shopDataSet.Товар". При необходимости она может быть перемещена или удалена.
            this.товарTableAdapter.Fill(this.shopDataSet1.Товар);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "shopDataSet.Товар_Корзина". При необходимости она может быть перемещена или удалена.
            
            // TODO: данная строка кода позволяет загрузить данные в таблицу "shopDataSet.Отзывы". При необходимости она может быть перемещена или удалена.

            // TODO: данная строка кода позволяет загрузить данные в таблицу "shopDataSet.Товар". При необходимости она может быть перемещена или удалена.
            this.товарTableAdapter.Fill(this.shopDataSet1.Товар);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "shopDataSet.Товар". При необходимости она может быть перемещена или удалена.
            this.товарTableAdapter.Fill(this.shopDataSet1.Товар);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "shopDataSet.Товар". При необходимости она может быть перемещена или удалена.
            this.товарTableAdapter.Fill(this.shopDataSet1.Товар);


        }
        private static Product f;

        public static Product fd
        {
            get
            {
                if (f == null || f.IsDisposed) f = new Product ();
                return f;
            }
        }
        public void ShowForm()
        {
            товарBindingSource.Position = 0;
            Show();
            Activate();
        }

        private void Product_Shown(object sender, EventArgs e)
        {
            товарBindingSource.Position = товарBindingSource.Find("id_товара", idCurrent);
        }

        private void товарDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }


      private void товарDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if ((товарDataGridView.Rows[e.RowIndex].Cells["dataGridViewTextBoxColumn3"].Value == null) || (товарDataGridView.Rows[e.RowIndex].Cells["dataGridViewTextBoxColumn3"].Value.ToString() == "Вентилятор"))
                 e.CellStyle.BackColor = Color.LightGreen;
            else
                    e.CellStyle.BackColor = Color.Pink;
        }

        private void товарBindingNavigatorSaveItem_Click_1(object sender, EventArgs e)
        {
            this.Validate();
            this.товарBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.shopDataSet1);

        }

        private void товарBindingNavigatorSaveItem_Click_2(object sender, EventArgs e)
        {
            this.Validate();
            this.товарBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.shopDataSet1);

        }

        private void товарBindingNavigatorSaveItem_Click_3(object sender, EventArgs e)
        {
            this.Validate();
            this.товарBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.shopDataSet1);

        }

        private void товарBindingNavigatorSaveItem_Click_4(object sender, EventArgs e)
        {
            this.Validate();
            this.товарBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.shopDataSet1);

        }


        string GetSelectedFieldName()
        {
            return
            товарDataGridView.Columns[товарDataGridView.CurrentCell.ColumnIndex
            ].DataPropertyName;
        }

        private void toolStripButtonFind_Click(object sender, EventArgs e)
        {
            if (toolStripTextBoxFind.Text == "")
            {
                MessageBox.Show("Вы ничего не задали", "Внимание",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            int indexPos;
            try
            {
                indexPos =
                товарBindingSource.Find(GetSelectedFieldName(),
                toolStripTextBoxFind.Text);
            }
            catch (Exception err)
            {
                MessageBox.Show("Ошибка поиска \n" + err.Message);
                return;
            }
            if (indexPos > -1)
                товарBindingSource.Position = indexPos;
            else
            {
                MessageBox.Show("Такого товара нет", "Внимание",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                товарBindingSource.Position = 0;
            }
        }

        private void checkBoxFind_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxFind.Checked)
            {
                if (toolStripTextBoxFind.Text == "")
                    MessageBox.Show("Вы ничего не задали", "Внимание",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    try
                    {
                        товарBindingSource.Filter =
                        GetSelectedFieldName() + "='" + toolStripTextBoxFind.Text + "'";
                    }
                    catch (Exception err)
                    {
                        MessageBox.Show("Ошибка фильтрации \n" +
                        err.Message);
                    }
            }
            else
                товарBindingSource.Filter = "";
            if (товарBindingSource.Count == 0)
            {
                MessageBox.Show("Нет такого");
                товарBindingSource.Filter = "";
                checkBoxFind.Checked = false;
            }
        }

        int idCurrent = -1;
        public int ShowSelectForm(int idProduct)
        {
            idCurrent = idProduct;
            ShowDialog();
            return (int)((DataRowView)товарBindingSource.Current)["id_Товара"];
        }

        private void bindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {

        }
    }
}
