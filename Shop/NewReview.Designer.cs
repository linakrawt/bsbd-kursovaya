﻿
namespace Shop
{
    partial class NewReview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label id_ОтзываLabel;
            System.Windows.Forms.Label логинLabel;
            System.Windows.Forms.Label текст_ОтзываLabel;
            System.Windows.Forms.Label датаLabel;
            System.Windows.Forms.Label id_ТовараLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewReview));
            this.shopDataSet1 = new Shop.ShopDataSet1();
            this.отзывыBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.отзывыTableAdapter = new Shop.ShopDataSet1TableAdapters.ОтзывыTableAdapter();
            this.tableAdapterManager = new Shop.ShopDataSet1TableAdapters.TableAdapterManager();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.логинTextBox = new System.Windows.Forms.TextBox();
            this.DateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.button1 = new System.Windows.Forms.Button();
            this.текстTextBox = new System.Windows.Forms.TextBox();
            this.фотографияPictureBox = new System.Windows.Forms.PictureBox();
            this.button2 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.idтовараTextBox = new System.Windows.Forms.TextBox();
            id_ОтзываLabel = new System.Windows.Forms.Label();
            логинLabel = new System.Windows.Forms.Label();
            текст_ОтзываLabel = new System.Windows.Forms.Label();
            датаLabel = new System.Windows.Forms.Label();
            id_ТовараLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.shopDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.отзывыBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.фотографияPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // id_ОтзываLabel
            // 
            id_ОтзываLabel.AutoSize = true;
            id_ОтзываLabel.Font = new System.Drawing.Font("GothicRus", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            id_ОтзываLabel.Location = new System.Drawing.Point(99, 19);
            id_ОтзываLabel.Name = "id_ОтзываLabel";
            id_ОтзываLabel.Size = new System.Drawing.Size(87, 17);
            id_ОтзываLabel.TabIndex = 1;
            id_ОтзываLabel.Text = "id Отзыва:";
            // 
            // логинLabel
            // 
            логинLabel.AutoSize = true;
            логинLabel.Font = new System.Drawing.Font("GothicRus", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            логинLabel.Location = new System.Drawing.Point(130, 53);
            логинLabel.Name = "логинLabel";
            логинLabel.Size = new System.Drawing.Size(56, 17);
            логинLabel.TabIndex = 3;
            логинLabel.Text = "Логин:";
            // 
            // текст_ОтзываLabel
            // 
            текст_ОтзываLabel.AutoSize = true;
            текст_ОтзываLabel.Font = new System.Drawing.Font("GothicRus", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            текст_ОтзываLabel.Location = new System.Drawing.Point(66, 85);
            текст_ОтзываLabel.Name = "текст_ОтзываLabel";
            текст_ОтзываLabel.Size = new System.Drawing.Size(120, 17);
            текст_ОтзываLabel.TabIndex = 5;
            текст_ОтзываLabel.Text = "Текст Отзыва:";
            // 
            // датаLabel
            // 
            датаLabel.AutoSize = true;
            датаLabel.Font = new System.Drawing.Font("GothicRus", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            датаLabel.Location = new System.Drawing.Point(135, 183);
            датаLabel.Name = "датаLabel";
            датаLabel.Size = new System.Drawing.Size(51, 17);
            датаLabel.TabIndex = 7;
            датаLabel.Text = "Дата:";
            // 
            // id_ТовараLabel
            // 
            id_ТовараLabel.AutoSize = true;
            id_ТовараLabel.Font = new System.Drawing.Font("GothicRus", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            id_ТовараLabel.Location = new System.Drawing.Point(107, 217);
            id_ТовараLabel.Name = "id_ТовараLabel";
            id_ТовараLabel.Size = new System.Drawing.Size(79, 17);
            id_ТовараLabel.TabIndex = 9;
            id_ТовараLabel.Text = "id Товара:";
            // 
            // shopDataSet1
            // 
            this.shopDataSet1.DataSetName = "ShopDataSet1";
            this.shopDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // отзывыBindingSource
            // 
            this.отзывыBindingSource.DataMember = "Отзывы";
            this.отзывыBindingSource.DataSource = this.shopDataSet1;
            // 
            // отзывыTableAdapter
            // 
            this.отзывыTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.UpdateOrder = Shop.ShopDataSet1TableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.ИсторияПокупок_logTableAdapter = null;
            this.tableAdapterManager.ИсторияПокупокTableAdapter = null;
            this.tableAdapterManager.ОтзывыTableAdapter = this.отзывыTableAdapter;
            this.tableAdapterManager.ПокупательTableAdapter = null;
            this.tableAdapterManager.ТоварTableAdapter = null;
            this.tableAdapterManager.ХарактеристикиTableAdapter = null;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.Location = new System.Drawing.Point(192, 13);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(200, 26);
            this.textBox1.TabIndex = 67;
            // 
            // логинTextBox
            // 
            this.логинTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.логинTextBox.Location = new System.Drawing.Point(192, 47);
            this.логинTextBox.Name = "логинTextBox";
            this.логинTextBox.ReadOnly = true;
            this.логинTextBox.Size = new System.Drawing.Size(200, 26);
            this.логинTextBox.TabIndex = 69;
            // 
            // DateTimePicker
            // 
            this.DateTimePicker.Enabled = false;
            this.DateTimePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DateTimePicker.Location = new System.Drawing.Point(192, 183);
            this.DateTimePicker.Name = "DateTimePicker";
            this.DateTimePicker.Size = new System.Drawing.Size(200, 22);
            this.DateTimePicker.TabIndex = 73;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(60)))), ((int)(((byte)(75)))));
            this.button1.Font = new System.Drawing.Font("GothicRus", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(182)))), ((int)(((byte)(184)))));
            this.button1.Location = new System.Drawing.Point(58, 385);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(111, 57);
            this.button1.TabIndex = 65;
            this.button1.Text = "Открыть фото";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // текстTextBox
            // 
            this.текстTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.текстTextBox.Location = new System.Drawing.Point(192, 79);
            this.текстTextBox.Multiline = true;
            this.текстTextBox.Name = "текстTextBox";
            this.текстTextBox.Size = new System.Drawing.Size(463, 98);
            this.текстTextBox.TabIndex = 71;
            // 
            // фотографияPictureBox
            // 
            this.фотографияPictureBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(86)))), ((int)(((byte)(123)))));
            this.фотографияPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.фотографияPictureBox.Location = new System.Drawing.Point(192, 258);
            this.фотографияPictureBox.Name = "фотографияPictureBox";
            this.фотографияPictureBox.Size = new System.Drawing.Size(200, 184);
            this.фотографияPictureBox.TabIndex = 64;
            this.фотографияPictureBox.TabStop = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(60)))), ((int)(((byte)(75)))));
            this.button2.Font = new System.Drawing.Font("GothicRus", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(182)))), ((int)(((byte)(184)))));
            this.button2.Location = new System.Drawing.Point(469, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(186, 57);
            this.button2.TabIndex = 75;
            this.button2.Text = "Сохранить отзыв";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // idтовараTextBox
            // 
            this.idтовараTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.idтовараTextBox.Location = new System.Drawing.Point(192, 211);
            this.idтовараTextBox.Name = "idтовараTextBox";
            this.idтовараTextBox.Size = new System.Drawing.Size(200, 26);
            this.idтовараTextBox.TabIndex = 76;
            // 
            // NewReview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(182)))), ((int)(((byte)(184)))));
            this.ClientSize = new System.Drawing.Size(677, 463);
            this.Controls.Add(this.idтовараTextBox);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.логинTextBox);
            this.Controls.Add(this.DateTimePicker);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.текстTextBox);
            this.Controls.Add(this.фотографияPictureBox);
            this.Controls.Add(id_ОтзываLabel);
            this.Controls.Add(логинLabel);
            this.Controls.Add(текст_ОтзываLabel);
            this.Controls.Add(датаLabel);
            this.Controls.Add(id_ТовараLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "NewReview";
            this.Text = "Создание отзыва";
            this.Load += new System.EventHandler(this.NewReview_Load);
            ((System.ComponentModel.ISupportInitialize)(this.shopDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.отзывыBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.фотографияPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ShopDataSet1 shopDataSet1;
        private System.Windows.Forms.BindingSource отзывыBindingSource;
        private ShopDataSet1TableAdapters.ОтзывыTableAdapter отзывыTableAdapter;
        private ShopDataSet1TableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox логинTextBox;
        private System.Windows.Forms.DateTimePicker DateTimePicker;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox текстTextBox;
        private System.Windows.Forms.PictureBox фотографияPictureBox;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox idтовараTextBox;
    }
}