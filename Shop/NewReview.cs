﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace Shop
{
    public partial class NewReview : Form
    {
        public NewReview()
        {
            InitializeComponent();
        }

        class DB
        {
            SqlConnection connection = new SqlConnection(Properties.Settings.Default.ShopConnectionString);

            public void OpenConnection()
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                    connection.Open();
            }

            public SqlConnection GetConnection()
            {
                return connection;
            }

            public void CloseConnection()
            {
                if (connection.State == System.Data.ConnectionState.Open)
                    connection.Close();
            }
        }
        public int idReview = 0;
        private void NewReview_Load(object sender, EventArgs e)
        {
            String Login = Auth.Personallogin;
            логинTextBox.Text = Login;
            // TODO: данная строка кода позволяет загрузить данные в таблицу "shopDataSet1.Отзывы". При необходимости она может быть перемещена или удалена.
            DB db = new DB();
            db.OpenConnection();
            SqlCommand command = new SqlCommand("SELECT Логин FROM Отзывы WHERE Логин LIKE @Login", db.GetConnection());
            command.Parameters.Add("Login", SqlDbType.NVarChar).Value = Login;
           
            SqlCommand command2 = new SqlCommand("SELECT MAX(id_Отзыва) FROM Отзывы", db.GetConnection());
            int Sql = Convert.ToInt32(command2.ExecuteScalar());
            Sql++;
            textBox1.Text = Sql.ToString();
            idReview = Sql;
        }


        public void ShowForm()
        {
            // toolStripButtonOk.Visible = false;
            // покупательBindingSource.Position = 0;
            Show();
            Activate();
        }
        private static NewReview f;
        public static NewReview fw
        {
            get
            {
                if (f == null || f.IsDisposed) f = new NewReview();
                return f;
            }
        }

        private void отзывыBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.отзывыBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.shopDataSet1);

        }

        private void текст_ОтзываTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (Injection.Check(текстTextBox.Text) == false || Injection.Check(idтовараTextBox.Text) == false)
            {
                MessageBox.Show("Некорректные символы!");
                return;
            }
            DB db = new DB();
            db.OpenConnection();

            String Login = Auth.Personallogin;
            if (логинTextBox.Text == "" || текстTextBox.Text == "" || idтовараTextBox.Text == "" )
            {
                MessageBox.Show("Заполнены не все обязательные поля!");
                return;
            }
            int id;
            if (!int.TryParse(idтовараTextBox.Text, out id))
                MessageBox.Show("Некорректные данные!");
            else
            {
                SqlCommand command1 = new SqlCommand("SELECT id_Товара FROM Отзывы WHERE id_Товара = " + idтовараTextBox.Text + " ", db.GetConnection());
                var Sql1 = command1.ExecuteScalar();


                if (Sql1 == null)
                {
                    MessageBox.Show("Нет такого товара!");
                    return;
                }
            }


            try
            {
                DateTime Date = DateTimePicker.Value;
                string Text = текстTextBox.Text;
                SqlCommand command2 = new SqlCommand("INSERT INTO Отзывы ( Логин, Текст_Отзыва, Дата, id_Товара) VALUES ( @Login, @Text, @Date, " + idтовараTextBox.Text + ")", db.GetConnection());
                command2.Parameters.Add("@Date", SqlDbType.DateTime).Value = Date;
                command2.Parameters.Add("@Login", SqlDbType.NVarChar).Value = Login;
                command2.Parameters.Add("@Text", SqlDbType.NVarChar).Value = Text;
                // command2.Parameters.Add("@Login", SqlDbType.NVarChar).Value = Login;
                //  SqlCommand command1 = new SqlCommand("UPDATE Покупатель SET Логин ='" + логинTextBox.Text + "' WHERE Логин LIKE @Login", db.GetConnection());



                var Sql2 = command2.ExecuteScalar();

                if (фотографияPictureBox.Image != null)
                {
                    byte[] imageData = null;
                    using (var ms = new MemoryStream())
                    {
                        фотографияPictureBox.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                        imageData = ms.ToArray();
                    }
                    SqlCommand command10 = new SqlCommand("UPDATE Отзывы SET Фотография = @imageDat WHERE id_Отзыва LIKE @idReview", db.GetConnection());
                    command10.Parameters.Add("@idReview", SqlDbType.NVarChar).Value = idReview;
                    command10.Parameters.Add("@imageDat", SqlDbType.VarBinary).Value = imageData;
                    var Sql10 = command10.ExecuteScalar();

                }

                if (Convert.ToInt32(Sql2) != null)
                    MessageBox.Show("Отзыв сохранен.");
                else MessageBox.Show("Что-то не так");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Что-то не так, "+ex.Message+"");
            }




        }

        string fileImage = "";
        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "Укажите файл для фото";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                fileImage = openFileDialog1.FileName;
                try
                {
                    фотографияPictureBox.Image = new
                    Bitmap(openFileDialog1.FileName);
                }
                catch
                {
                    MessageBox.Show("Выбран не тот формат файла", "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else fileImage = "";
        }
    }
}
