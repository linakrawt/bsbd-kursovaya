﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;

namespace Shop
{



    public partial class Auth : Form
    {

        private string Hash(string input)
        {
            using (SHA256Managed sha256 = new SHA256Managed())
            {
                var hash = sha256.ComputeHash(Encoding.UTF8.GetBytes(input));
                var sb = new StringBuilder(hash.Length * 2);
                foreach (byte b in hash)
                {
                    sb.Append(b.ToString("x2"));
                }
                return sb.ToString();
            }
        }

        public static String Personallogin;
        public class DB
        {
            SqlConnection connection = new SqlConnection(Properties.Settings.Default.ShopConnectionString);

            public void OpenConnection()
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                    connection.Open();
            }

            public SqlConnection GetConnection()
            {
                return connection;
            }

            public void CloseConnection()
            {
                if (connection.State == System.Data.ConnectionState.Open)
                    connection.Close();
            }
        }

        public Auth()
        {
            InitializeComponent();
        }

        private void Auth_Load(object sender, EventArgs e)
        {

        }

        private void enterButton_Click(object sender, EventArgs e)
        {
            if (logintextBox.Text == "" && passwordtextBox.Text == "")
            {
                MessageBox.Show("Вы не ввели логин и/или пароль!");
            }
            else
            {
                if (Injection.Check(logintextBox.Text)==false|| Injection.Check(passwordtextBox.Text)== false)
                {
                    MessageBox.Show("Некорректные символы!");
                    return;
                }
                Personallogin = logintextBox.Text;
                String Personalpassword = Hash(passwordtextBox.Text);


                DB db = new DB();


                SqlCommand command = new SqlCommand("SELECT * FROM Покупатель WHERE Логин = @Login AND Пароль = @Password", db.GetConnection());
                command.Parameters.Add("@Login", SqlDbType.VarChar).Value = Personallogin;
                command.Parameters.Add("@Password", SqlDbType.VarChar).Value = Personalpassword;


                db.OpenConnection();
                //var Sql = command.ExecuteScalar();


                if (command.ExecuteScalar() != null)
                {
                    if (Personallogin == "admin")
                    {
                        Hide();
                        var MainForm = new MainForm();
                        MainForm.Closed += (s, args) => this.Close();
                        MainForm.Show();
                    }
                    else
                    {
                        Hide();
                        var MainFormUser = new MainFormUser();
                        MainFormUser.Closed += (s, args) => this.Close();
                        MainFormUser.Show();
                    }
                }

                else MessageBox.Show("Ошибка при вводе логина и/или пароля. Проверьте правильность введённых данных.");

                db.CloseConnection();
            }
        }
       
    }
}
