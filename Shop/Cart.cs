﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Shop.ProductUser;
using System.Data.SqlClient;

namespace Shop
{
    public partial class Cart : Form
    {

        public class itemCart
        {
            public int idProduct { set; get; }
            public float Price { set; get; }
            public int Amount { set; get; }
            public float Cost { set; get; }
            public itemCart(int product, float productPrice, int productAmount, float productCost) {
                idProduct = product;
                Price = productPrice;
                Amount = productAmount;
                Cost = productCost;
            }

        }

        public Cart()
        {
            InitializeComponent();
        }


        class DB
        {
            SqlConnection connection = new SqlConnection(Properties.Settings.Default.ShopConnectionString);

            public void OpenConnection()
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                    connection.Open();
            }

            public SqlConnection GetConnection()
            {
                return connection;
            }

            public void CloseConnection()
            {
                if (connection.State == System.Data.ConnectionState.Open)
                    connection.Close();
            }
        }


        private void Cart_Load(object sender, EventArgs e)
        {




            DataTable dt = new DataTable();
            dt.Columns.Add("id Товара");
            dt.Columns.Add("Цена единицы");
            dt.Columns.Add("Количество");
            dt.Columns.Add("Стоимость");
            foreach (var oItem in cart)
            {
                dt.Rows.Add(new object[] { oItem.idProduct, oItem.Price, oItem.Amount, oItem.Cost });
            }

            dt.Columns["id Товара"].ReadOnly = true;
            dt.Columns["Цена единицы"].ReadOnly = true;
            //dt.Columns["Стоимость"].ReadOnly = true;
            dataGridView1.DataSource = dt;

            // dataGridView1.ReadOnly = true;
            // dataGridView1.DataSource = cart;

            double balans = 0;
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                double incom;
                double.TryParse((row.Cells[3].Value ?? "0").ToString().Replace(".", ","), out incom);
                balans += incom;
            }
            label2.Text = balans.ToString();

        }


        void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var grid = sender as DataGridView;
            var rowIdx = (e.RowIndex + 1).ToString();

            var centerFormat = new StringFormat()
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };

            var headerBounds = new Rectangle(e.RowBounds.Left, e.RowBounds.Top, grid.RowHeadersWidth, e.RowBounds.Height);
            e.Graphics.DrawString(rowIdx, this.Font, SystemBrushes.ControlText, headerBounds, centerFormat);
        }


        public void ShowForm()
        {
            // toolStripButtonOk.Visible = false;
            // покупательBindingSource.Position = 0;
            Show();
            Activate();
        }
        private static Cart f;
        public static Cart fw
        {
            get
            {
                if (f == null || f.IsDisposed) f = new Cart();
                return f;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try { 
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                DB db = new DB();
                db.OpenConnection();
                DateTime Date = DateTime.Now;
                string Login = Auth.Personallogin;
                int idProduct = Convert.ToInt32(dataGridView1.Rows[i].Cells["id Товара"].Value);
                float Cost = float.Parse((dataGridView1.Rows[i].Cells["Стоимость"].Value ?? "0").ToString());
                int Amount = Convert.ToInt32(dataGridView1.Rows[i].Cells["Количество"].Value);

                SqlCommand command = new SqlCommand("SELECT MAX(id_Покупки) FROM ИсторияПокупок", db.GetConnection());
                int Sql = Convert.ToInt32(command.ExecuteScalar());
                ++Sql;
                SqlCommand command1 = new SqlCommand("INSERT INTO ИсторияПокупок (  Дата, Стоимость, Логин, id_Товара, Количество) VALUES (@Date, @Cost, @Login, @idProduct, @Amount)", db.GetConnection());

                command1.Parameters.Add("idProduct", SqlDbType.Int).Value = idProduct;
                command1.Parameters.Add("@Date", SqlDbType.DateTime).Value = Date;
                command1.Parameters.Add("@Cost", SqlDbType.Money).Value = Cost;
                command1.Parameters.Add("@Login", SqlDbType.NVarChar).Value = Login;
                //command1.Parameters.Add("@Sql", SqlDbType.Int).Value = Sql;
                command1.Parameters.Add("@Amount", SqlDbType.Int).Value = Amount;


                // command2.Parameters.Add("@Login", SqlDbType.NVarChar).Value = Login;
                //  SqlCommand command1 = new SqlCommand("UPDATE Покупатель SET Логин ='" + логинTextBox.Text + "' WHERE Логин LIKE @Login", db.GetConnection());

                var Sql2 = command1.ExecuteScalar();

                if (Convert.ToInt32(Sql2) != null)
                    MessageBox.Show("Товары оплачены");

                else MessageBox.Show("Что-то не так");

            }

            cart.Clear();
        }
        catch (Exception err)
            {
                MessageBox.Show("Что-то пошло не так, "+err.Message+"");
            }
        
            
        }

        private void DataGridView1_CellValueChanged( object sender, DataGridViewCellEventArgs e)
        {
            //for (int i = 0; i < dataGridView1.Rows.Count; i++)
            //{
            //    if (e.ColumnIndex < 0 || e.RowIndex < 0) return;
            //    float a = float.Parse((dataGridView1.Rows[i].Cells["Цена единицы"].Value ?? "0").ToString());
            //    int b = Convert.ToInt32((dataGridView1.Rows[i].Cells["Количество"].Value ?? "0"));

            //    dataGridView1.Rows[i].Cells["Стоимость"].Value = a * b;
            //}
              
                double balans = 0;
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {



               
                double incom;
                double.TryParse((row.Cells[3].Value ?? "0").ToString().Replace(".", ","), out incom);
                balans += incom;
            }
            label2.Text = balans.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {

            try
            {
                for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                {

                    float a = float.Parse((dataGridView1.Rows[i].Cells["Цена единицы"].Value ?? "0").ToString());

                    int id;
                    if (!int.TryParse(dataGridView1.Rows[i].Cells["Количество"].Value.ToString(), out id) || Convert.ToInt32((dataGridView1.Rows[i].Cells["Количество"].Value)) < 0)
                    {
                        MessageBox.Show("Некоректное значение столбца!", "Внимание",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    int b = Convert.ToInt32((dataGridView1.Rows[i].Cells["Количество"].Value ?? "0"));

                    dataGridView1.Rows[i].Cells["Стоимость"].Value = a * b;
                }
            }
            catch (Exception err)
            {
                MessageBox.Show("Что-то пошло не так, " + err.Message + "");
            }
        }
    }
}
