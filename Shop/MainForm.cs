﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Shop
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
                Close();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = MessageBox.Show("Вы хотите закрыть программу?",
           "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Question) !=
            DialogResult.Yes;
        }

        private void справкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("(c) ТУСУР,КИБЭВС, Кравцова Эвелина Евгеньевна, группа 728-1, 2021", "О программе",
              MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void выходToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            выходToolStripMenuItem_Click(sender, e);
        }

        private void справкаToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            справкаToolStripMenuItem_Click(sender, e);
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void MainForm_ContextMenuStripChanged(object sender, EventArgs e)
        {

        }

        private void menuStrip2_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void покупательToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Customer.fw.ShowForm();

        }

        private void характеристикиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Features.fw.ShowForm();
        }

        private void товарToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Product.fd.ShowForm();
        }

   

       
        private void покупкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PurchaseHistory.fw.ShowForm();
        }

      

       

        private void отзывыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Reviews.fw.ShowForm();
        }

        private void запросыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormSQL.fw.ShowForm();
        }

        private void файлToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void историяПокупокLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HistotyLog.fw.ShowForm();
        }

        private void историяПокупокToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PurchaseHistory.fw.ShowForm();
        }
    }
}
