﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using static Shop.ShopDataSet;

namespace Shop
{
    public partial class CustomerUser : Form
    {

        private string Hash(string input)
        {
            using (SHA256Managed sha256 = new SHA256Managed())
            {
                var hash = sha256.ComputeHash(Encoding.UTF8.GetBytes(input));
                var sb = new StringBuilder(hash.Length * 2);
                foreach (byte b in hash)
                {
                    sb.Append(b.ToString("x2"));
                }
                return sb.ToString();
            }
        }

        public CustomerUser()
        {
            InitializeComponent();
        }

        private void покупательBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.покупательBindingSource.EndEdit();
          

        }

        private void CustomerUser_Load(object sender, EventArgs e)
        {
            String Login = Auth.Personallogin;
            логинTextBox.Text = Login;
            String Password = null;
            String Mail = null;
            DateTime Date ;
            ComboBox Gender = null;
            String Number = null;
            String Name1 = null;
            String Name2 = null;
            String Name3 = null;
             PictureBox Pic = null;

            DB db = new DB();
            db.OpenConnection();
            SqlCommand command = new SqlCommand("SELECT Дата_рождения FROM Покупатель WHERE Логин LIKE @Login", db.GetConnection());
            command.Parameters.Add("Login", SqlDbType.NVarChar).Value = Login;

            var Sql = command.ExecuteScalar();
            Date = Convert.ToDateTime(Sql);
            дата_рожденияDateTimePicker.Value = Date;

            SqlCommand command1 = new SqlCommand("SELECT Электронная_почта FROM Покупатель WHERE Логин LIKE @Login", db.GetConnection());
            SqlCommand command2 = new SqlCommand("SELECT Пол FROM Покупатель WHERE Логин LIKE @Login", db.GetConnection());
            SqlCommand command3 = new SqlCommand("SELECT Телефон FROM Покупатель WHERE Логин LIKE @Login", db.GetConnection());
            SqlCommand command4 = new SqlCommand("SELECT Фамилия FROM Покупатель WHERE Логин LIKE @Login", db.GetConnection());
            SqlCommand command5 = new SqlCommand("SELECT Имя FROM Покупатель WHERE Логин LIKE @Login", db.GetConnection());
            SqlCommand command6 = new SqlCommand("SELECT Отчество FROM Покупатель WHERE Логин LIKE @Login", db.GetConnection());
            SqlCommand command7 = new SqlCommand("SELECT Фотография FROM Покупатель WHERE Логин LIKE @Login", db.GetConnection());



            command1.Parameters.Add("@Login", SqlDbType.NVarChar).Value = Convert.ToString(Login);
            command2.Parameters.Add("@Login", SqlDbType.NVarChar).Value = Convert.ToString(Login);
            command3.Parameters.Add("@Login", SqlDbType.NVarChar).Value = Convert.ToString(Login);
            command4.Parameters.Add("@Login", SqlDbType.NVarChar).Value = Convert.ToString(Login);
            command5.Parameters.Add("@Login", SqlDbType.NVarChar).Value = Convert.ToString(Login);
            command6.Parameters.Add("@Login", SqlDbType.NVarChar).Value = Convert.ToString(Login);
            command7.Parameters.Add("@Login", SqlDbType.NVarChar).Value = Convert.ToString(Login);


            var Sql1 = command1.ExecuteScalar();
            var Sql2 = command2.ExecuteScalar();
            var Sql3 = command3.ExecuteScalar();
            var Sql4 = command4.ExecuteScalar();
            var Sql5 = command5.ExecuteScalar();
            var Sql6 = command6.ExecuteScalar();
            var Sql7 = command7.ExecuteScalar();


            электронная_почтаTextBox.Text = Convert.ToString(Sql1);

            if (Sql2 != null)
                полComboBox.Text = Sql2.ToString();


            телефонTextBox.Text = Convert.ToString(Sql3);

            фамилияTextBox.Text = Convert.ToString(Sql4);

            имяTextBox.Text = Convert.ToString(Sql5);


            if (Sql6 != null)
                отчествоTextBox.Text = Convert.ToString(Sql6);

            if (Sql7.ToString() != null)
            {

                    MemoryStream stmBLOBData = new MemoryStream((byte[])Sql7);
                    фотографияPictureBox.Image = Image.FromStream(stmBLOBData);



            }

        }

        private void полTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void фотографияPictureBox_Click(object sender, EventArgs e)
        {

        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
        }

        string fileImage = "";
        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "Укажите файл для фото";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                fileImage = openFileDialog1.FileName;
                try
                {
                    фотографияPictureBox.Image = new Bitmap(openFileDialog1.FileName);
                }
                catch
                {
                    MessageBox.Show("Выбран не тот формат файла", "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else fileImage = "";
        }



        public void ShowForm()
        {
           
            Show();
            Activate();
        }


        private static CustomerUser f;
        public static CustomerUser fw
        {
            get
            {
                if (f == null || f.IsDisposed) f = new CustomerUser();
                return f;
            }
        }

        private void toolStripButtonOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }


        private void логинTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        class DB
        {
            SqlConnection connection = new SqlConnection(Properties.Settings.Default.ShopConnectionString);

            public void OpenConnection()
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                    connection.Open();
            }

            public SqlConnection GetConnection()
            {
                return connection;
            }

            public void CloseConnection()
            {
                if (connection.State == System.Data.ConnectionState.Open)
                    connection.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (Injection.Check(электронная_почтаTextBox.Text) == false || Injection.Check(телефонTextBox.Text) == false || Injection.Check(фамилияTextBox.Text) == false
                || Injection.Check(имяTextBox.Text) == false || Injection.Check(отчествоTextBox.Text) == false)
            {
                MessageBox.Show("Некорректные символы!");
                return;
            }

            String Login = Auth.Personallogin;
            if (логинTextBox.Text == "" || электронная_почтаTextBox.Text == "" || телефонTextBox.Text == "" || фамилияTextBox.Text == "" || имяTextBox.Text == "")
            {
                MessageBox.Show("Заполнены не все обязательные поля!");
                return;
            }

            DB db = new DB();
            db.OpenConnection();
            SqlCommand command1 = new SqlCommand("UPDATE Покупатель SET Логин ='" + логинTextBox.Text + "' WHERE Логин LIKE @Login", db.GetConnection());
            command1.Parameters.Add("@Login", SqlDbType.NVarChar).Value = Login;
           


            SqlCommand command3 = new SqlCommand("UPDATE Покупатель SET Электронная_почта ='" + электронная_почтаTextBox.Text + "' WHERE Логин LIKE @Login", db.GetConnection());
            command3.Parameters.Add("@Login", SqlDbType.NVarChar).Value = Login;

            SqlCommand command4 = new SqlCommand("UPDATE Покупатель SET Дата_рождения ='" + дата_рожденияDateTimePicker.Value + "' WHERE Логин LIKE @Login", db.GetConnection());
            command4.Parameters.Add("@Login", SqlDbType.NVarChar).Value = Login;

            SqlCommand command5 = new SqlCommand("UPDATE Покупатель SET Пол ='" + полComboBox.Text + "' WHERE Логин LIKE @Login", db.GetConnection());
            command5.Parameters.Add("@Login", SqlDbType.NVarChar).Value = Login;

            SqlCommand command6 = new SqlCommand("UPDATE Покупатель SET Телефон ='" + телефонTextBox.Text + "' WHERE Логин LIKE @Login", db.GetConnection());
            command6.Parameters.Add("@Login", SqlDbType.NVarChar).Value = Login;

            SqlCommand command7 = new SqlCommand("UPDATE Покупатель SET Фамилия ='" + фамилияTextBox.Text + "' WHERE Логин LIKE @Login", db.GetConnection());
            command7.Parameters.Add("@Login", SqlDbType.NVarChar).Value = Login;

            SqlCommand command8 = new SqlCommand("UPDATE Покупатель SET Имя ='" + имяTextBox.Text + "' WHERE Логин LIKE @Login", db.GetConnection());
            command8.Parameters.Add("@Login", SqlDbType.NVarChar).Value = Login;

            SqlCommand command9 = new SqlCommand("UPDATE Покупатель SET Отчество ='" + отчествоTextBox.Text + "' WHERE Логин LIKE @Login", db.GetConnection());
            command9.Parameters.Add("@Login", SqlDbType.NVarChar).Value = Login;
            if (фотографияPictureBox.Image != null)
            {
                byte[] imageData = null;
                using (var ms = new MemoryStream())
                {
                    фотографияPictureBox.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    imageData = ms.ToArray();
                }
                SqlCommand command10 = new SqlCommand("UPDATE Покупатель SET Фотография = @imageDat WHERE Логин LIKE @Login", db.GetConnection());
                command10.Parameters.Add("@Login", SqlDbType.NVarChar).Value = Login;
                command10.Parameters.Add("@imageDat", SqlDbType.VarBinary).Value = imageData;
                    var Sql10 = command10.ExecuteScalar();
               
            }

            var Sql1 = command1.ExecuteScalar();

            var Sql3 = command3.ExecuteScalar();
            var Sql4 = command4.ExecuteScalar();
            var Sql5 = command5.ExecuteScalar();
            var Sql6 = command6.ExecuteScalar();
            var Sql7 = command7.ExecuteScalar();
            var Sql8 = command8.ExecuteScalar();
            var Sql9 = command9.ExecuteScalar();
            


            if ((Convert.ToInt32(Sql1) != null) &&(Convert.ToInt32(Sql3) != null) && (Convert.ToInt32(Sql4) != null) 
                && (Convert.ToInt32(Sql6) != null) && (Convert.ToInt32(Sql7) != null) && (Convert.ToInt32(Sql8) != null)   )
                MessageBox.Show("Изменения сохранены.");
            else MessageBox.Show("Что-то не так");
        }

        private void парольTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void дата_рожденияDateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            дата_рожденияDateTimePicker.MaxDate = DateTime.Today;
        }

        private void button3_Click(object sender, EventArgs e)
        {

            if (Injection.Check(парольTextBox.Text) == false || Injection.Check(textBox2.Text) == false || Injection.Check(textBox1.Text)==false)
            {
                MessageBox.Show("Некорректные символы!");
                return;
            }

            string Login = Auth.Personallogin;
            DB db = new DB();
            db.OpenConnection();

            string Personalpassword = Hash(парольTextBox.Text);

            SqlCommand command = new SqlCommand("SELECT * FROM Покупатель WHERE Логин = @Login AND Пароль = @Password", db.GetConnection());
            command.Parameters.Add("@Login", SqlDbType.VarChar).Value = Login;
            command.Parameters.Add("@Password", SqlDbType.VarChar).Value = Personalpassword;
            if (парольTextBox.Text == "" || textBox2.Text == "" || textBox1.Text == "")
            {
                MessageBox.Show("Введены не все данные!");
                return;
            }
            try
            {
                if ((command.ExecuteScalar() != null) && (textBox2.Text == textBox1.Text))
                {

                    SqlCommand command2 = new SqlCommand("UPDATE Покупатель SET Пароль ='" + Hash(textBox1.Text) + "' WHERE Логин LIKE @Login", db.GetConnection());
                    command2.Parameters.Add("@Login", SqlDbType.NVarChar).Value = Login;
                    var Sql2 = command2.ExecuteScalar();
                    MessageBox.Show("Пароль был изменен.");

                }
                else
                {
                    MessageBox.Show("Старый пароль введен неверно и/или пароли не совпадают!!");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Что-то не так");
            }
            



        }
    }
}

