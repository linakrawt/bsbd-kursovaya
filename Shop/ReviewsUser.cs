﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shop
{
    public partial class ReviewsUser : Form
    {


        public void ShowForm()
        {
            // toolStripButtonOk.Visible = false;
            // покупательBindingSource.Position = 0;
            Show();
            Activate();
        }
        private static ReviewsUser f;
        public static ReviewsUser fw
        {
            get
            {
                if (f == null || f.IsDisposed) f = new ReviewsUser();
                return f;
            }
        }
        public ReviewsUser()
        {
            InitializeComponent();
        }

        private void ReviewsUser_Load(object sender, EventArgs e)
        {
            this.отзывыTableAdapter.Fill(this.shopDataSet.Отзывы);
        }

        private void отзывыBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.отзывыBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.shopDataSet);
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {

        }

        private void отзывыDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
           NewReview.fw.ShowForm();
        }
    }
}
